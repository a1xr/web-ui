/* eslint-disable import/no-extraneous-dependencies */

import axios from 'axios';

import { API_BASE_URL } from '../constants';

const SEARCH_URI = API_BASE_URL + 'exchange/assetlookup/';

export const GET_ASSETS = 'ASSETS_LOAD';
export const GET_ASSETS_SUCCESS = 'ASSETS_LOAD_SUCCESS';
export const GET_ASSETS_FAIL = 'ASSETS_LOAD_FAIL';

export default function reducer(state = { options: [] }, action) { // setting the initial state
  switch (action.type) {
    case GET_ASSETS:
      return { ...state, isLoading: true };
    case GET_ASSETS_SUCCESS:
      return { ...state, isLoading: false, options: action.payload };
    case GET_ASSETS_FAIL:
      return {
        ...state,
        isLoading: false,
        error: 'Error while fetching ASSETS'
      };
    default:
      return state;
  }
}


export function lookup() {
    return function(dispatch) {
        dispatch({type: GET_ASSETS});
        axios.get(SEARCH_URI)
        .then(response => {
            var options = response.data.map(value=> value.symbol);
            dispatch({
                type: GET_ASSETS_SUCCESS,
                payload: options
            });
        })
        .catch((error) => {
            dispatch({type: GET_ASSETS_FAIL});
        })
    }
}
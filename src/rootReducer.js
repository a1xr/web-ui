/*
 src/reducers/rootReducer.js
*/
import { combineReducers } from 'redux';
import assetLookup from './data/assetLookup';

export default combineReducers({
 assetLookup
});
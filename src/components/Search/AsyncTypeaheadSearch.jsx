import React, {Fragment} from "react";
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';

import { connect } from 'react-redux';
import {lookup} from '../../data/assetLookup';
import MenuItem from './MenuItem';

class AsyncTypeaheadSearch extends React.Component {
  
    render() {

      var params = this._getFormattedParams();

      return (
        <Fragment>
          <AsyncTypeahead
            {...params}
            labelKey="login"
            minLength={3}
            onSearch={this._handleSearch}
            placeholder="Search for a Github user..."
            renderMenuItemChildren={(option, props) => (
              <MenuItem option={option} />
            )}
          />
        </Fragment>
      );
    }

    _getFormattedParams = ()=>{
      return {
        allowNew: this.props.allowNew, 
        isLoading: this.props.isLoading, 
        multiple: this.props.multiple, 
        options: this.props.options 
      };

    };
  
    _handleChange = (e) => {
      const {checked, name} = e.target;
      this.setState({[name]: checked});
    }
  
    _handleSearch = (query) => {
      this.setState({isLoading: true});
      this.props.lookup(query);
    }
  }

  const mapStateToProps = state => {

    return {
      allowNew: false,
      isLoading: !!state.assetLookup.isLoading,
      multiple: false,
      options: state.assetLookup.options
    };
  };
  
  const mapDispatchToProps = {
    lookup
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AsyncTypeaheadSearch);
import { Link } from "react-router-dom";
import React from 'react';


const id = 0;
const MenuItem = ({option}) => {
    var path = '/' + option;
    return (
        <Link to={path}>{option}</Link>
    )
};

export default MenuItem;